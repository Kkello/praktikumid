package praktikum9;
import lib.TextIO;
public class Palindroom {
	public static void main(String[] args) {
		System.out.print("Sisesta midagi: ");
		String s = TextIO.getlnString();
		if (onPalindroom(s)) {
			System.out.println("On palindroom");
		} else {
			System.out.println("Ei ole");
		}
	}
	private static boolean onPalindroom(String s) {
		for (int i = 0; i < s.length() / 2; i++) {
			char taht1 = s.charAt(i);
			char taht2 = s.charAt(s.length() - i - 1);
			// System.out.println(taht1);
			// System.out.println(taht2);
			if (taht1 != taht2) {
				return false;
			}
		}
		return true;
	}
}